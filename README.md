# Anomaly Detection in Cyber-Physical Energy Systems

This repository contains implemented models for anomaly detection in Cyber-Physical Energy Systems. Anomalies within
a multi-agent system for scheduling distributed energy systems were investigated. Different architectures were chosen:
a centralized anomaly detection, which considers the data of the complete agent system and a distributed architecture
which only considers the data for one certain agent.
For each architecture, the following approaches were investigated: Isolation Forest, Support Vector Machine and
an Autoencoder.
The used data sets can be found here: https://zenodo.org/record/7934270

For the distributed anomaly detection, the datasets had to be filled every second, so time-based data is mimicked.
Therefore, these data sets can be found at "filled data sets".

The project is structured as follows: models for the distributed architecture are stored in "distributed", models
for the centralized architecture stored in "centralized".
Since different types of anomalies were investigated (anomalies in the values of the exchanged messages between the
agents and anomalies in the communication behavior of the agents), the models are given accordingly.

For each approach, the features and hyperparameter that lead to the best performance are given.

For the Graph-Deviation Network, install it from here: https://github.com/d-ailin/GDN

Afterwards, include the given data sets into the installed GDN. For the centralized anomaly detection, add the data
from here from centralized/communication/GDN (comm_1m_8996_all and comm_15m_2012_all) to the installation of the GDN to
the folder "data". For the distributed anomaly detection, add the data from here from distributed/communication/GDN
(comm_1m_8996 and comm_15m_2012) to the folder "data" in the installed GDN. Then run the code.
The best parameters for the centralized anomaly detection are: topk 4, lr 0.0001, batch 512, val ratio 0.1.
The best parameters for the distributed anomaly detection are: topk4, lr 0.001, batch 512, val ratio 0.1.
Run the code using the following command or via run.sh.

``python main.py -device cpu -dataset comm_15m_2012_all -topk 4 -random_seed 5 -batch 512 -dim 64 -out_layer_num 1 -slide_stride 1 -out_layer_inter_dim 128 -val_ratio 0.1 -decay 0 -epoch 100``

Follow the installation and execution guidelines provided in the repository of the GDN.

Regarding the implementation of the Isolation Forest and the SVM, parameters were selected using a grid search.
Also for the GDN, different parameters were analyzed. Different architectures and methods to determine the
threshold for anomalies were investigated for the autoencoder. The number of neurons for the input and output layer was
chosen depending on the number of features. The best selected parameters regarding the models are listed in the
following.

Isolation Forest
| Architecture | Contamination | Features | Samples |
| ------------ | ------------- | -------- | ------- |
| Centralized  |       0.5     |    0.8   |   0.6   |
| Distributed  |   0.09, 0.02  |     1    |   0.8   |

Support Vector Machine
| Architecture | Kernel |    Gamma   |  n  |
| ------------ | ------ | ---------- |  -- |
| Centralized  |  rbf   |    0.001   | 0.1 |
| Distributed  |  rbf   | 1/features | 0.5 |

Autoencoder
| Architecture | Learning Rate |  Layers | Batch Size |
| ------------ | ------------- | ------- |  --------- |
| Centralized  |     0.01      | [4,2,4] |     64     |
| Distributed  |     0.001     | [3,2,3] |     64     |

Graph-Deviation Network
| Architecture | Learning Rate |  Batch Size | TopK |
| ------------ | ------------- | ----------- |  --------- |
| Centralized  |     0.0001    |     512     |     64     |
| Distributed  |     0.01      |     512     |     64     |
