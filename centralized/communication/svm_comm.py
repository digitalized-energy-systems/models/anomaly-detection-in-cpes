import pickle
from os.path import abspath
from pathlib import Path

import pandas as pd
from sklearn import svm
from sklearn.metrics import confusion_matrix, \
    f1_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer, MinMaxScaler

# Adapt path accordingly
ROOT_PATH = Path(abspath(__file__)).parent.parent.parent / 'normal_data'
ab = Path(abspath(__file__)).parent.parent.parent / 'anomalous_data' / 'comm'
# best results regarding the anomalies in the communication behavior with date, agent
features = ['date', 'agent',
            # 'agent0_12hours_sent',
            # 'agent1_12hours_sent',
            # 'agent2_12hours_sent', 'agent3_12hours_sent',
            # 'agent4_12hours_sent'
            ]


def create_classifier(kernel, gamma='scale', nu=0.5):
    # best results regarding the anomalies in the communication behavior with kernel rbf, gamma 0.001 and n = 0.1
    clf = svm.OneClassSVM(kernel='rbf', gamma=0.001, nu=0.1)

    X_train = []  # enter training data here
    X_train = pd.concat(X_train)
    X_train.fillna(-1, inplace=True)

    y_train = X_train.pop("anomaly").astype(int)
    print('start fitting')
    X_train = X_train[
        features]
    pipeline = Pipeline([('normalizer', Normalizer()),
                         ('scaler', MinMaxScaler())])
    pipeline.fit(X_train)

    X_train_transformed = pipeline.transform(X_train)

    clf.fit(X_train_transformed)

    filename = 'svm_comm.sav'
    pickle.dump(clf, open(filename, 'wb'))

    return clf, pipeline


def _make_prediction(clf, X, y):
    print('start predicting')
    predictions = clf.predict(X)
    print(predictions)

    for idx in range(len(y)):
        if y[idx] == 1:
            y[idx] = 0
    for idx in range(len(y)):
        if y[idx] == -1:
            y[idx] = 1

    ctr = 0

    for idx in range(len(predictions)):
        if predictions[idx] == 1:
            predictions[idx] = 0
    for idx in range(len(predictions)):
        if predictions[idx] == -1:
            ctr += 1
            predictions[idx] = 1
    print(ctr)
    try:
        tn, fp, fn, tp = confusion_matrix(Y, predictions).ravel()
    except:
        print(confusion_matrix(Y, predictions))
        return None, None, None, None, None
    print(tn, fp, fn, tp)

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    print('acc', accuracy)
    precision = tp / (tp + fp)
    print('prec', precision)
    sensitivity = tp / (tp + fn)
    print('sens', sensitivity)
    specificity = tn / (tn + fp)
    print('spec', specificity)
    f_one = f1_score(Y, predictions)
    print('f1 ', f_one)

    print('------------------------')
    return accuracy, precision, sensitivity, specificity, f_one


paths = []  # add paths to test data here

clf, pipeline = create_classifier(kernel='rbf')
for pth in paths:
    print(pth)
    X = pd.read_csv(
        pth,
        delimiter=',',
        encoding="utf-8-sig")

    X.fillna(-1, inplace=True)

    Y = X.pop('anomaly').astype(int)
    X = X[features]

    X = pipeline.transform(X)

    accuracy, precision, sensitivity, specificity, f_one = _make_prediction(
        clf, X, Y)
