import pandas as pd
from sklearn.ensemble import IsolationForest
from sklearn.metrics import recall_score, precision_score, f1_score, \
    confusion_matrix
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

# best results regarding the anomalies in the values with date, agent, value, msg_type, sender
# and when available: max, min or 'is_below_max'
features = ['date', 'agent', 'value', 'msg_type', 'sender']


def create_classifier():
    # best results regarding the anomalies in the values with contamination 0.3, max_features 3,0, max_samples 1,0
    model = IsolationForest(n_estimators=50, random_state=0, contamination=0.3,
                            max_features=3, max_samples=1.0)
    X_train = []

    X_train = pd.concat(X_train)

    X_train.fillna(-1, inplace=True)

    y_train = X_train.pop("anomaly").astype(int)

    X_train.fillna(-1, inplace=True)
    X_train = X_train[features]

    X_train = MinMaxScaler().fit_transform(X_train)
    model.fit(X_train)
    scores = model.decision_function(X_train)

    return model


def _make_prediction(clf, pth):
    X = [
        pd.read_csv(
            pth,
            delimiter=',',
            encoding="utf-8-sig"),
    ]

    X = pd.concat(X)

    X.fillna(-1, inplace=True)

    Y = X.pop('anomaly').astype(int)
    X = X[features]

    X = MinMaxScaler().fit_transform(X)

    scores = clf.decision_function(
        X)

    clf.fit(X)
    predictions = clf.predict(X)

    normal = Y.loc[Y == 1]
    normal_index = list(normal.index)
    print(len(normal_index))

    u_normal = Y.loc[Y == -1]
    u_normal = list(u_normal.index)
    print(len(u_normal))

    plt.figure(figsize=(12, 8))
    plt.hist(scores, bins=50)

    Y = [0 if c == 1 else c for c in Y]
    Y = [1 if c == -1 else c for c in Y]

    predictions = list(predictions)
    for idx in range(len(predictions)):
        if predictions[idx] == 1:
            predictions[idx] = 0
    for idx in range(len(predictions)):
        if predictions[idx] == -1:
            predictions[idx] = 1

    tn, fp, fn, tp = confusion_matrix(Y, predictions).ravel()
    print('tn ', tn, 'fp ', fp, 'fn ', fn, 'tp ', tp)
    accuracy = (tp + tn) / (tp + tn + fp + fn)
    print('acc', accuracy)
    precision = tp / (tp + fp)
    print('prec', precision)
    sensitivity = tp / (tp + fn)
    print('sens', sensitivity)
    specificity = tn / (tn + fp)
    print('spec', specificity)

    f_1 = f1_score(Y, predictions)
    print('f1 score ', f_1)

    precision_macro = precision_score(Y, predictions, average='macro',
                                      zero_division=0)
    precision_weighted = precision_score(Y, predictions,
                                         average='weighted',
                                         zero_division=0)
    prec = precision_score(Y, predictions)
    recall_macro = recall_score(Y, predictions, average='macro',
                                zero_division=0)
    recall_weighted = recall_score(Y, predictions,
                                   average='weighted', zero_division=0)
    print(
        f'Precision (macro): {precision_macro}, '
        f'Precision (weighted): {precision_weighted}'
        f' prec {prec}')
    print(
        f'Recall (macro): {recall_macro}, '
        f'Recall (weighted): {recall_weighted}')
    print('------------------------')
    return [precision_macro, precision_weighted, recall_macro,
            recall_weighted, f_1]


clf = create_classifier()
pathes = []  # enter path to data here
for pth in pathes:
    print(pth)
    _make_prediction(clf, pth)
