import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer, MinMaxScaler
from sklearn.metrics import f1_score
import matplotlib.pyplot as plt
from sklearn.metrics import (confusion_matrix)

# best results regarding the anomalies in the values with date, agent, value, msg_type, sender
# and when available: max, min or 'is_below_max'
features = ['date', 'value', 'sender', 'agent', 'msg_type', 'neighbor', 'anomaly']
# best setting regarding anomalies in the values: learning rate = 0.001

X_train = []  # add training data here
X_train = pd.concat(X_train)
X_train.fillna(-1, inplace=True)

y_train = X_train.pop("anomaly").astype(int)
X_train = X_train[features]

y_train = [0 if c == 1 else c for c in y_train]
y_train = [1 if c == -1 else c for c in y_train]

# https://www.kaggle.com/robinteuwens/anomaly-detection-with-auto-encoders
pipeline = Pipeline([('normalizer', Normalizer()),
                     ('scaler', MinMaxScaler())])
pipeline.fit(X_train)

X_train_transformed = pipeline.transform(X_train)

# data dimensions // hyperparameters
input_dim = X_train_transformed.shape[1]
BATCH_SIZE = 128
EPOCHS = 15

# https://keras.io/layers/core/
inputArray = tf.keras.Input(shape=(input_dim), )
# shape layers according to number of features (here: 6)
encoded = tf.keras.layers.Dense(6, activation='relu')(inputArray)
encoded = tf.keras.layers.Dense(4, activation='sigmoid')(encoded)
encoded = tf.keras.layers.Dense(2, activation='sigmoid')(encoded)
encoded = tf.keras.layers.Dense(4, activation='sigmoid')(encoded)
decoded = tf.keras.layers.Dense(6, activation='sigmoid')(encoded)
decoded = tf.keras.layers.Dense(input_dim, activation='relu')(decoded)
autoencoder = tf.keras.Model(inputArray, decoded)

# https://keras.io/api/models/model_training_apis/
lr = 0.001
opt = tf.keras.optimizers.Adam(learning_rate=lr)
autoencoder.compile(optimizer=opt,
                    loss="mse",
                    metrics=["acc"])

# print an overview of our model
autoencoder.summary()
history = autoencoder.fit(
    X_train_transformed, X_train_transformed,
    shuffle=True,
    epochs=EPOCHS,
    batch_size=BATCH_SIZE,
    verbose=1,
    validation_data=(X_train_transformed, X_train_transformed))

reconstructions = autoencoder.predict(X_train_transformed)
mse = np.mean(np.power(X_train_transformed - reconstructions, 2), axis=1)


def mad_score(points):
    """https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm """
    m = np.median(points)
    ad = np.abs(points - m)
    mad = np.median(ad)

    return 0.6745 * ad / mad


z_scores = mad_score(mse)

print(max(z_scores), max(mse))

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# autoencoder.save(f'{lr}_value_with_mxa_sender_value')
X_test = []  # add test data here

X_test = pd.concat(X_test)
X_test.fillna(-1, inplace=True)

y_test = X_test.pop("anomaly")
y_test = [0 if c == 1 else c for c in y_test]
y_test = [1 if c == -1 else c for c in y_test]
X_test = X_test[features]

X_test_transformed = pipeline.transform(X_test)
# pass the transformed test set through the autoencoder to get the reconstructed result
reconstructions = autoencoder.predict(X_test_transformed)

# calculating the mean squared error reconstruction loss per row in the numpy array
# mse = np.mean(np.abs(X_test_transformed - reconstructions), axis=1)
mse = np.mean(np.power(X_test_transformed - reconstructions, 2), axis=1)


def mad_score(points):
    """https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm """
    m = np.median(points)
    ad = np.abs(points - m)
    mad = np.median(ad)

    return 0.6745 * ad / mad


z_scores = mad_score(mse)

THRESHOLDS = [0.1, 0.25, 0.3, 0.5, 0.75, 0.9, 0.95, 1, 1.15, 1.25, 1.5, 1.75, 2, 2.5, 3,
              3.5, 4,
              4.25,
              4.5, 4.75, 5, 5.25, 5.5, 5.75, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
              20, 25, 30, 35, 40, 45, 50, 55, 60, 75, 80]
for t in THRESHOLDS:
    outliers = z_scores > t
    cm = confusion_matrix(y_test, outliers)

    # true/false positives/negatives
    (tn, fp,
     fn, tp) = cm.flatten()
    print('THRESHOLD ', t)

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    print('tp ', tp, 'tn ', tn, 'fp ', fp, 'fn ', fn)

    print('acc', accuracy)
    precision = tp / (tp + fp)
    print('prec', precision)
    sensitivity = tp / (tp + fn)
    print('sens', sensitivity)
    specificity = tn / (tn + fp)
    print('spec', specificity)

    print('f_1', f1_score(y_test, outliers))
    fp_list = []
